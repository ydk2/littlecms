<?php
session_start();
if (!isset($_SESSION['inicjuj']))
{
session_regenerate_id();
$_SESSION['inicjuj'] = true;
$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
}
if($_SESSION['ip'] != $_SERVER['REMOTE_ADDR'])
{
?>
<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type"/>
	<link href="<?=dirname(TEN);?>/style/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
session_destroy();
echo "<div>Zweryfikuj dane! <a href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."/login.php\"><button>Weryfikuj</button></a></div>";
exit();  
?>
</body></html>
<?php    
}
if(!isset($_SESSION['uzytkownik']))
{
// Sesja się zaczyna, wiec inicjujemy użytkownika anonimowego
$_SESSION['uzytkownik'] = 0;
}
if($_SESSION['uzytkownik'] > 0)
{
$start=microtime(); 
function __autoload($klasa) {
	$nazwa=explode('\\', $klasa); 
	$a=end($nazwa);
    require "core/" . $a . ".class.php"; 
}
$n = new CMS\EdytujPliki();	
?>
<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type"/>
	<link href="<?=dirname(TEN);?>/style/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php

echo "<div style=\"border: 1px black solid; padding:3px; color:#4c87dd; \" >{$n->plik} ".$n->langLista()."</div>";
echo "<b>&nbsp;{$n->lang['lang']}&nbsp; info:{$_SERVER[PATH_INFO]}</b><hr />";
echo "<div style=\"widht:98%;padding:5px;\">";
echo "<div style=\"border: 1px black solid;padding:5px;float:left;\">{$n->formularz('nowy')}</div>";
echo "<div style=\"border: 1px black solid;padding:5px;float:left;\">{$n->formularz('wgraj')}</div>";
echo "<div style=\"padding:5px;float:right;\">Zalogowany, {$_SESSION['name']} <a href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."/login.php?wyloguj=tak\"><button>Wyjdź</button></a> ";
echo "<a href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."/login.php\"><button>Zarządaj kontem</button></a></div>"; 
echo "</div>";
echo "<hr style=\"border: 1px black solid;clear:left;\" />";
echo "<div id=\"lista\">".$n->splitPath()."</div>";
echo "<hr>";
echo "<div style=\"width:100%;\">";
echo "<div style=\"width:98%;padding:7px;border: 1px black solid;\">{$n->formularz('usun')} {$n->formularz('przenies')} ".get_current_user()." ".fileowner($n->plik)."</div>";
echo "<div style=\"border: 1px black solid;width:49%;float:left;padding:3px;\"><b>{$n->lang['katalog']}</b> ";
echo $n->pokazListe();
echo "</div>";
echo "<div style=\"border: 1px black solid;float:left;width:49%;padding:3px;\"><b>{$n->lang['plik']}</b>";
echo $n->pokazListe(PLIK);
echo "</div>";
echo "</div>";
echo "<div style=\"width:98%;border: 1px black solid;clear:both;padding:7px;\">{$n->szukajForm(1)}";
echo "<div style=\"border: 1px black solid;padding:5px;float:both;\">{$n->formularz('nazwa')}{$n->chmodForm()}</div></div>";
if(isset($_REQUEST['szukaj'])){
	echo "<div style=\"width:98%;border: 1px black solid;clear:both;padding:7px;\">{$n->szukajForm(2)}</div>";
} else {
	echo "<div style=\"width:98%;height:500px;border: 1px black solid;overflow:auto;padding:7px;\">{$n->pokazPlik()}</div>";
}
echo "<hr style=\"clear:both;\">";
function d_free($path){
    $bytes = disk_free_space($path); 
     $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
     $base = 1024;
     $class = min((int)log($bytes , $base) , count($si_prefix) - 1);
     return sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . ' ';
}

function czas($stop,$start)
{
	return round($stop-$start,4)." s.";
}

$stop=microtime();
echo "<p style=\"border: 1px black solid;padding:3px;color:#4c87dd;text-align:center;\">".czas($stop,$start)." ".d_free($n->path)."</p>";
echo "</body></html>";
$n->koniec();
}
else
{
	//header('location:http://'.dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME]).'/login.php');
	?>
<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type"/>
	<link href="<?=dirname(TEN);?>/style/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
	echo "<div style=\"width:100%; height:100%;background-color:gray;padding:20px;\"><div style=\"background-color:white;border: 1px black solid;padding:5px;width:200px;height:100px;margin-left:auto; margin-right:auto;text-align:center;\">Zaloguj się! <a href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."/login.php\"><button>Zaloguj</button></a></div>";
?>
</body></html>
<?php	
}
 

?>
