var _co = ''; //set domain URL
var _obrazek = _co + '../galeria/runa.gif';//set image path
var _tekst = '<b>Wgrywam...</b>';//set loading message
var _objekt = false;

//create the XMLHttpRequest
function _socket(){
	if (window.XMLHttpRequest){ // if Mozilla, Safari etc
		_objekt = new XMLHttpRequest();
	}else if (window.ActiveXObject){ // if IE
		try{
			_objekt = new ActiveXObject("Msxml2.XMLHTTP");
		}catch (e){
			try{
				_objekt = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e){			
			}
		}
	}else{	
		_objekt = false;
	}
	return _objekt;
}
//get content via GET
function _pobierz(url, containerid){
	var _objekt = _socket();	
	document.getElementById(containerid).innerHTML = '<img src="' + _obrazek + '" />' + _tekst;
	_objekt.onreadystatechange=function(){
		wgraj_str(_objekt, '', containerid);
	}
	_objekt.open('GET', url, true);
	_objekt.send(null);
}

function wgraj_str(_objekt, content, containerid){
	if ( _objekt.readyState == 4 && _objekt.status == 200 ){
		
		document.getElementById(containerid).innerHTML = _objekt.responseText;
	}
}

//functions for posted values from forms via POST
function _pobierz_post(url, content, containerid){
	var xinput = content;
	var _objekt = _socket();
	document.getElementById(containerid).innerHTML = '<img src="' + _obrazek + '" />' + _tekst;
	_objekt.open('POST', url, true);
	_objekt.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	_objekt.onreadystatechange = function() {
		wgraj_str(_objekt, content, containerid);
    }
    _objekt.send(content);
}
//convert form data sent to POST CONTENT
function wyslij_form(page_to,form_name,containerid) {
	var content = _zamien_form(form_name);
	_pobierz_post(page_to, content, containerid);
}

function _zamien_form(form_y){
	var f=document.getElementById(form_y);
	var content_to_submit = '';
	var form_element;
	var last_element_name = '';
	for (i = 0; i < f.elements.length; i++){
    	form_element = f.elements[i];
		switch (form_element.type){
			// text fields, hidden form elements
			case 'text':
			case 'hidden':
			case 'email':
			case 'file':
			case 'password':
			case 'textarea':
			case 'select-one':
			content_to_submit += form_element.name + '=' + form_element.value + '&';
			break;
			// radio buttons
			case 'radio':
				if (form_element.checked){
				  content_to_submit += form_element.name + '=' + form_element.value + '&';
				}
			break;
			// checkboxes
			case 'checkbox':
				if (form_element.checked){
				  	// Continuing multiple, same-name checkboxes
					if (form_element.name == last_element_name){
						// Strip of end ampersand if there is one
						if (content_to_submit.lastIndexOf('&') == content_to_submit.length - 1){
						  content_to_submit = content_to_submit.substr(0, content_to_submit.length - 1);
						}
						// Append value as comma-delimited string
						content_to_submit += ',' + form_element.value;
					}else{
						content_to_submit += form_element.name + '=' + form_element.value;
					}
					content_to_submit += '&';
					last_element_name = form_element.name;
				}
			break;
		}//end switch
	} //end for
	// remove trailing separator
	content_to_submit = content_to_submit.substr(0, content_to_submit.length - 1);
	return content_to_submit;
}