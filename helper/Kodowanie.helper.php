<?php
namespace Helper {
/**
 * kodowanie piliku ini konfig users.
 */
class Kodowanie {

function encode_this($string,$save)
{
	$convert = base64_encode(strToHex(str_replace(array("\n","="), array("@#$%","*&^%"), $string)));
	if (isset($save)) {
		file_put_contents($save, $convert);
	}
	return $convert;
}
function decode_this($coded)
{
	$convert=str_replace(array("@#$%","*&^%"), array("\n","="), hexToStr(base64_decode($coded)));
	return $convert;
}
function hexToStr($hex)
{
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2)
    {
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}
 
function strToHex($string)
{
    $hex='';
    for ($i=0; $i < strlen($string); $i++)
    {
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}
} // koniec Kodowanie
} // helper koniec
?>
<?php
namespace {
echo "<hr>\n";

echo htmlentities(encode_this(file_get_contents("../logindata.ini.php"), "./testcode"))."\n";
echo "<hr>\n";
echo htmlentities(decode_this(file_get_contents("./testcode")))."\n";

echo "<hr>\n";
}
?>