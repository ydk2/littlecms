<?php
namespace Helper {
/**
 * 
 */
class wgraj {
	
	function __construct($skad,$miejsce_zapisu=null,$max=NULL,$ini=NULL) {
		$this->skad = $skad;
		$wgrana_wielkosc = (int) $_SERVER['CONTENT_LENGTH'];
		$ini = ($ini===NULL)?realpath('./Wgraj.helper.gui.lang'):$ini;

		$alerty = parse_ini_file($ini,TRUE);
		$default = $alerty['lista']['default'];
		$lang= $alerty[$default];
		$miejsce_zapisu = ($miejsce_zapisu == NULL)?$alerty['lista']['uploaddir']:$miejsce_zapisu;
		$this->miejsce_zapisu = $miejsce_zapisu;
		//$max = $this->Rozmiar_do_Byte($max);
		if ($max === NULL) {
			$max = $this->Rozmiar_do_Byte($alerty['lista']['limit']);
		}
		if($max > $this->Rozmiar_do_Byte(ini_get('upload_max_filesize'))) {
			$max = $this->Rozmiar_do_Byte(ini_get('upload_max_filesize'));
		}
		$this->wgrana_wielkosc = $wgrana_wielkosc;
		$this->maxi = $max;
		$this->alerty = $lang;
		$pliki = $_FILES[$skad];
		$this->nazwa = $pliki['name'];
		$this->rozmiar = $pliki['size'];
		$this->typ = $pliki['type'];
		$this->tmp = $pliki['tmp_name'];
		$this->blad= $pliki['error'];
	}
	function pokazRozmiar($filesize){
		if(is_numeric($filesize)){
		$decr = 1024; $step = 0;
		$prefix = array('Byte','KB','MB','GB','TB','PB');
		while(($filesize / $decr) > 0.99){
			$filesize = $filesize / $decr;
			$step++;
		} 
			return round($filesize,2).' '.$prefix[$step];
		} else {
		return 'NaN';
		}
	} // pokaz rozmiar w string
	
	function Rozmiar_do_Byte ($rozmiar) {
		$incr=1024;
		$getBit = substr($rozmiar, strlen((int) $rozmiar));
		$getBit = (preg_match("/(b|byte)$/", strtolower($getBit)))?substr($getBit, 0,-1):$getBit;
		$lista=preg_match("/(K|M|G|T|P)$/", strtoupper($getBit), $match);
		switch ($match[0]) {
			case 'K':
				return (int) $rozmiar * $incr; 
				break;
			case 'M':
				return (int) $rozmiar * pow($incr,2); 
				break;
			case 'G':
				return (int) $rozmiar *  pow($incr,3); 
				break;
			case 'T':
				return (int) $rozmiar *  pow($incr,4); 
				break;		
			case 'P':
				return (int) $rozmiar *  pow($incr,5); 
				break;
			default:
				return (int) $rozmiar; 
				break;
		}
	}
	function pokazBlad($nr)
	{
		return $this->alerty['blad'.$nr];
	}
	function WgrajPliki($code=0)
	{
	if ($this->wgrana_wielkosc > $this->maxi) {
	$arr_out[].= "Maksymalny rozmiar ".$this->pokazRozmiar($this->maxi)." został przekroczony!\n
	Rozmiar przekazanego/nych pliku/ów to: ".$this->pokazRozmiar($this->wgrana_wielkosc);
	} else {
	foreach ($this->nazwa as $num => $plik) {
	if ($this->blad[$num] !== UPLOAD_ERR_OK ) {
	$arr_out[].= $this->pokazBlad($this->blad[$num]);
	} elseif($this->blad[$num] === UPLOAD_ERR_OK) {
		if($code === 1){
			$zapis=base64_encode($this->nazwa[$num]);
		} else {
			$zapis = $this->nazwa[$num];
		} 
		if (move_uploaded_file($this->tmp[$num], $this->miejsce_zapisu.DIRECTORY_SEPARATOR.$zapis)) {
		$arr_out[].=  $plik;	
	} else {
		$arr_out[].=  "{$this->pokazBlad($this->blad[$num])} {$this->nazwa[$num]}";
	}
	}
	}
	}
	return $arr_out;
}
}
} // koniec helper
?>
<?php
namespace {


//upload_max_filesize

}
?>