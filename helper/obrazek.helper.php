<?php
namespace Helper {
	class Obrazek {
		public function __construct($obrazek = NULL) {
			$this -> obrazek = $obrazek;
			$info = getimagesize($this -> obrazek);
			$this -> typ = $info[2];
			$this -> nowy_w = $info[0];
			$this -> nowy_h = $info[1];
			return TRUE;
		}

		public function __destruct() {
			foreach ($this as $key => $value) {
				unset($this -> $key);
			}
			clearstatcache();
		}

		private function zmien() {
			if (!extension_loaded('gd') && !extension_loaded('gd2')) {
				trigger_error("GD is not loaded", E_USER_WARNING);
				return false;
			}
			$nowa_nazwa = $this -> nowa_nazwa;
			switch ($this->typ) {
				case 1 :
					$im = imagecreatefromgif($this -> obrazek);
					break;
				case 2 :
					$im = imagecreatefromjpeg($this -> obrazek);
					break;
				case 3 :
					$im = imagecreatefrompng($this -> obrazek);
					break;
				default :
					trigger_error('Unsupported filetype!', E_USER_WARNING);
					break;
			}
			if ($this -> n_w == NULL and $this -> n_h == NULL) {
				$this -> n_w = $this -> nowy_w;
				$this -> n_h = $this -> nowy_h;
			}
			$obrazek_tmp = imagecreatetruecolor($this -> n_w, $this -> n_h);
			/* Check if this image is PNG or GIF, then set if Transparent*/
			if (($this -> typ == 1) OR ($this -> typ == 3)) {
				imagealphablending($obrazek_tmp, false);
				imagesavealpha($obrazek_tmp, true);
				$przezroczystosc = imagecolorallocatealpha($obrazek_tmp, 255, 255, 255, 127);
				imagefilledrectangle($obrazek_tmp, 0, 0, $this -> n_w, $this -> n_h, $przezroczystosc);
			}
			imagecopyresampled($obrazek_tmp, $im, 0, 0, 0, 0, $this -> n_w, $this -> n_h, $this -> nowy_w, $this -> nowy_h);
			switch ($this->typ) {
				case 1 :
					imagegif($obrazek_tmp, $nowa_nazwa);
					break;
				case 2 :
					imagejpeg($obrazek_tmp, $nowa_nazwa);
					break;
				case 3 :
					imagepng($obrazek_tmp, $nowa_nazwa);
					break;
				default :
					trigger_error('Failed resize image!', E_USER_WARNING);
					break;
			}
			imagedestroy($obrazek_tmp);
			return $nowa_nazwa;
		}

		public function Rozmiar($w = 100, $h = 100, $force = 0) {
			if ($force == 1) {
				$nowy_w = $w;
				$nowy_h = $h;
			} else {
				if ($this -> nowy_w <= $w && $this -> nowy_h <= $h) {
					$nowy_w = $this -> nowy_w;
					$nowy_h = $this -> nowy_h;
				} else {
					if ($w / $this -> nowy_w > $h / $this -> nowy_h) {
						$nowy_w = $w;
						$nowy_h = $this -> nowy_h * ($w / $this -> nowy_w);
					} else {
						$nowy_w = $this -> nowy_w * ($h / $this -> nowy_h);
						$nowy_h = $h;
					}
				}
				$nowy_w = round($nowy_w);
				$nowy_h = round($nowy_h);
			}
			$this -> n_w = $nowy_w;
			$this -> n_h = $nowy_h;
		}

		public function Zapisz($nowa_nazwa = NULL) {
			$this -> nowa_nazwa = $nowa_nazwa;
			header_remove();
			header('Content-type:' . mime_content_type($this -> obrazek));
			$pokaz = $this -> zmien();
			exit($pokaz);
		}

		public function Zamknij() {
			$this -> __destruct();
		}

	}

}
namespace {
	$obrazek = $_REQUEST['img'];
	$ini= parse_ini_file('../konfig/konfig.ini');
	$pokaz = new Helper\Obrazek($obrazek);
	$pokaz -> Rozmiar($ini['width'], $ini['height'], $ini['force']);
	$pokaz -> Zapisz();
	$pokaz -> Zamknij();
}
?>