<?php
function write_ini($plik,$sekcja,$klucz,$zmien,$add=null){
	$ini_str=file_get_contents($plik);
	$ini = explode("\n", $ini_str);
	$aini=parse_ini_string($ini_str,TRUE);
	if (!$aini) {
		trigger_error("Is not ini file", E_USER_DEPRECATED);
	}
	foreach ($ini as $klucz_ini) {
		$ignoruj= preg_match("/^\;/", $klucz_ini);
		$sekcja_find= preg_match("/^\[+$sekcja+\]/", $klucz_ini);
		$inna_find= preg_match("/^\[+[a-zA-Z0-9_-]+\]/", $klucz_ini);
		$i=0;
		if($sekcja_find) {
			$out .= "$klucz_ini\n";
			$bini=$aini[$sekcja];
			foreach ($bini as $klucz_ini_str => $wartosc) {
				switch ($klucz_ini_str) {
				case $klucz:
				($add == DEL)?$out.='':$out.="$klucz_ini_str='$zmien'\n";
				$i++;
				break;
				default:
				$out.="$klucz_ini_str='$wartosc'\n";
				$i++;
				break;
				}
			}
				if ($add==ADD AND !array_key_exists($klucz, $bini)) {
				$adds="$klucz='$zmien'\n";
				$out.=$adds;
			}
		} elseif (!$sekcja_find AND $inna_find) {
			$out .= "$klucz_ini\n";
			$bini=$aini[substr($klucz_ini, 1, -1)];
			foreach ($bini as $klucz_ini_str => $wartosc) {
				$out.="$klucz_ini_str='$wartosc'\n";
			}
		} elseif ($ignoruj) {
			$out.="$klucz_ini\n";
		} 
	}
	clearstatcache();
	return file_put_contents($plik, $out);
}
function uzytkownicy($plik,$sekcja,$szukany,$podane){
	$ini=parse_ini_file($plik,TRUE);
	$ini_users=$ini[$sekcja];
	foreach ($ini_users as $uzytkownik => $haslo) {
		if ($uzytkownik == $szukany AND $haslo==$podane) {
			return TRUE;
		} else {
			$out=FALSE;
			continue;
		}
	}
	return $out;
}
session_start();
 ?>
<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type"/>
	<link href="<?=dirname(TEN);?>/style/style.css" rel="stylesheet" type="text/css" />
</head>
<body><div style="border: 1px black solid;padding:5px; text-align: center">
<?php
if (!isset($_SESSION['inicjuj']))
{
session_regenerate_id();
$_SESSION['inicjuj'] = true;
$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
}
if($_SESSION['ip'] != $_SERVER['REMOTE_ADDR'])
{
session_destroy();
echo "<div>Zweryfikuj dane! <a href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."/login.php\"><button>Weryfikuj</button></a></div>";
exit();        
}
$path = realpath("./logindata.ini.php");
$a=parse_ini_file($path,TRUE);
$b=$a['users'];
//write_ini($path,'usersdata','admin',base64_encode('email=ydk2@post.com|@|lang=pl'),ADD);
?>
<hr>
<?php
if(!isset($_SESSION['uzytkownik']))
{
// Sesja się zaczyna, wiec inicjujemy użytkownika anonimowego
$_SESSION['uzytkownik'] = 0;
}
if($_SESSION['uzytkownik'] > 0)
{
// Ktos jest zalogowany
if ($_GET['wyloguj']=='tak') {
	
	echo "Pa pa, {$_SESSION['name']}! <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Powrót</button></a>";
	//session_write_close();
	session_destroy();
}elseif ($_GET['usunkonto']=='tak') {
	echo "<p><h2>Potwierdź decyzję</h2></p><a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}?napewno=tak\"><button>Napewno</button></a>";
	echo "&nbsp;<a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Cofnij</button></a>";
}
elseif ($_GET['napewno']=='tak') {
	require 'helper/OperacjePliki.helper.php';
	$ini=parse_ini_file(realpath('./konfig/konfig.ini'),TRUE);
	\Helper\OperacjePliki::usun(realpath($ini['core']['home']).DIRECTORY_SEPARATOR.$_SESSION['name']);
	write_ini($path,'users',$_SESSION['name'],null,DEL);
	
	echo "<p><h2>Usunięto konto {$_SESSION['name']} i wylogowano</h2></p> <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Wróć</button></a>";
	session_destroy();
} elseif (isset($_POST['starehaslo']) AND isset($_POST['nowehaslo'])) {
	
	if ($stare = uzytkownicy($path,'users',$_SESSION['name'], sha1($_POST['starehaslo'])) !== false AND strlen($_POST['nowehaslo'])>=3){
	if (write_ini($path,'users',$_SESSION['name'],sha1($_POST['nowehaslo']))) {
		echo "Zmieniono hasło <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Odśwież</button></a>";
	} else {
		echo "Błąd! <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Powtórz</button></a>";
	}
} else {
	echo "Podałes nieprawidlowe hasło! <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Powtórz</button></a>";
}
}
else {
	echo "<h2>{$_SESSION['name']} zarządzaj swoim kontem</h2>";
	echo "<div style=\"text-align:left;width:400px;margin-left:30%;\">";
	echo "<p>Wejdź na stronę serwisu <a style=\"text-align:right;margin-left:30%;\" href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."\"><button>Wejdź</button></a></p>"; 
	echo "<p>Opuść serwis <a style=\"text-align:right;margin-left:47%;\" href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}?wyloguj=tak\"><button>Wyloguj</button></a></p>";
	echo "<p>Usuń konto (tej operacji nie można cofnąć) <a style=\"text-align:right;margin-left:0%;\" href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}?usunkonto=tak\"><button>Usuń konto</button></a></p>";
	echo "</div>";
	echo "<div style=\"text-align:left;width:400px;margin-left:30%;\">
	<p><b>Zmień swoje hasło</b></p>
	<form action=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\" method=\"POST\">
	podaj stare <input type=\"password\" name=\"starehaslo\" /><br />
	podaj nowe <input type=\"password\" name=\"nowehaslo\" /><br />
	<input type=\"submit\" value=\"zmień hasło\" />
	</form>
	</div>";
} 

}
else
{
 // Niezalogowany
if(isset($_POST['login']) AND isset($_POST['haslo']))
{
if(($id = uzytkownicy($path,'users',$_POST['login'], sha1($_POST['haslo']))) !== false)
{
// Logujemy uzytkownika, wpisal poprawne dane
$_SESSION['uzytkownik'] = $id;
$_SESSION['name']=$_POST['login'];
echo "Dziekujemy, jesteś zalogowany! <a href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."\"><button>Dalej</button></a>";
}
else
{
echo "Podales nieprawidlowe dane! <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Powtórz</button></a>";
}               
}
else
{
echo "<div>
<b>Zaloguj się: </b><br />
<form method=\"post\" action=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\">
Nazwa: <input type=\"text\" name=\"login\"/><br />
Hasło: <input type=\"password\" name=\"haslo\"/><br />
<input type=\"submit\" value=\"OK\"/></form></div>";  
echo "<hr /><div>
<b>Zarejestruj się: </b><br />
<form method=\"post\" action=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\">
Nowa nazwa: <input type=\"text\" name=\"newlogin\"/><br />
Nowe hasło: <input type=\"password\" name=\"newhaslo\"/><br />
<input type=\"submit\" value=\"OK\"/></form></div>";            
}       
}
if (isset($_POST['newlogin']) AND isset($_POST['newhaslo'])) {
	if (strlen($_POST['newlogin'])>=3 AND strlen($_POST['newhaslo'])>=3 AND !array_key_exists($_POST['newlogin'], $b)){
	if (write_ini($path,'users',$_POST['newlogin'],sha1($_POST['newhaslo']),ADD)) {
		$ini=parse_ini_file(realpath('./konfig/konfig.ini'),TRUE);
		mkdir(realpath($ini['core']['home']).DIRECTORY_SEPARATOR.$_POST['newlogin'],0777);
		echo "Teraz możesz się zalogować! ";//<a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Zaloguj</button></a>";
	} else {
		echo "Błąd! <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Powtórz</button></a>";
	}
} else {
	echo "Taki użytkownik już istnieje! <a href=\"http://{$_SERVER[HTTP_HOST]}{$_SERVER[SCRIPT_NAME]}\"><button>Powtórz</button></a>";
}
}

?>
</div>
</body></html>