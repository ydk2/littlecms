<?php
/**
 *
 */
namespace CMS;
class Pliki {
	public function __construct($path = '') {
		session_start();
		ob_start();
		static $NAZWA;
		$praca = getcwd();
		$this->DOM= $_SERVER['DOCUMENT_ROOT'];
		$this->URI= 'http://' . $_SERVER['HTTP_HOST'];
		$this->TEN= 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
		$this->TU= $_SERVER['REQUEST_URI'];
		$this->NAZWA = $_SERVER['SERVER_NAME'];
		$this->LINK= $_SERVER['PATH_INFO'];
		$this->PRACA = $praca;
		$konfig = parse_ini_file($this->PRACA . "/konfig/konfig.ini", true);
		$this -> konfig = $konfig;
		if ($path == '') {
			$path = $this -> konfig['core']['home'].DIRECTORY_SEPARATOR.$_SESSION['name'];
		}
		if ($_SESSION['name']=='admin') {
			$path = $this -> konfig['core']['home'];
		}
		if (!file_exists($path)) { ?>
			<!DOCTYPE HTML>
			<html>
			<head>
			<meta content="text/html; charset=UTF-8" http-equiv="content-type"/>
			<link href="<?=dirname(TEN);?>/style/style.css" rel="stylesheet" type="text/css" />
			</head>
			<body>
			<?php
			echo "Brak lokalizacji <a href=\"http://".dirname($_SERVER[HTTP_HOST].$_SERVER[SCRIPT_NAME])."/login.php\"><button>Wróć</button></a></body></html>";
			exit;
		}
		if (!isset($_SESSION['lang']) or $_SESSION['lang'] == '') {
			$langl = $this -> konfig['core']['lang'];
			$_SESSION['lang'] = $this -> konfig['core']['lang'];
		} elseif ($_REQUEST['lang'] != '') {
			$langl = $_REQUEST['lang'];
		} else {
			$langl = $_SESSION['lang'];
		}
		$langs = parse_ini_file($this->PRACA . "/langs/$langl.lang", TRUE);
		$lang = $langs[$langl];
		$this -> lang = $lang;
		$path = realpath($path);
		$this -> path = $path;
		$this -> plik = $this -> path . $this->LINK;
		$this->arr_lista=NULL;
		chdir($this -> path);
		
	}

	public function __destruct() {
		foreach ($this as $key => $value) {
			unset($this -> $key);
		}
		clearstatcache();
		ob_end_flush();		
	}

	public function koniec() {
		exit ($this->__destruct());
	}

	public function langLista($lang = '') {
		$path = ($this->LINK) ? $this->TEN . $this->LINK : $this->TEN;
		if (empty($lang)) {
			If (!isset($_REQUEST['lang']) or $_REQUEST['lang'] == '') {
				$lang = $this -> konfig['core']['lang'];
			} else {
				$_SESSION['lang'] = $_REQUEST['lang'];
			}
		}
		$form = array("<form action=\"$path\" method=\"POST\">",
    		"<select name=\"lang\">",
    		"<option value=\"$list\">$list_f</option>",
    		"</select>",
    		"<input type=\"submit\" name=\"wybierz\" value=\"{$this->lang['zmień']} {$this->lang['język']}\" />\n",
    		"</form>"
        );
		$opcje = $form[0] . $form[1];
		foreach (glob($this->PRACA.'/langs/*.lang') as $list) {
			$list_n = pathinfo($list, PATHINFO_FILENAME);
			$list_f = parse_ini_file($list);
			$opcje .= "<option value=\"$list_n\">{$list_f['lang']}</option>";
		}
		$opcje .= $form[3] . $form[4] . $form[5];
		return "$opcje";
	}

	public function lista($path = '') {
		$path = realpath($path);
		if ($path == '' or $path < $this -> path) {
			$path = $this -> path;
		}
		$p = "{" . $path . "/*," . $path . "/.*}";
		foreach (glob("$p",GLOB_BRACE) as $plik) {
			if (basename($plik) != '..' && basename($plik) != '.') {
				$info[basename(realpath($plik))] .= realpath($plik);
			}
		}  
		 
		 /*if ($lista = opendir($path)) {
		while (false !== ($plik = readdir($lista))) {
		if ($plik != "." && $plik != "..") {
			$info[basename(realpath($path.DIRECTORY_SEPARATOR.$plik))] .= realpath($path.DIRECTORY_SEPARATOR.$plik);
		}
		}
		closedir($lista);
		}*/
		
		return $info;
	}

	public function rekursywnie($path = '') {
		$a=array();
		$path = realpath($path);
		if ($path == '' or $path < $this -> path) {
			$path = $this -> path;
		}
		$dir[] = $path;
		while (count($dir) != 0) {
			$v = array_shift($dir);
			foreach (glob($v) as $item) {
				if (is_dir($item)) {
					$dir[] = $item . "/*";
					$a[$item] .= basename($item);
				} elseif (is_file($item)) {
					$a[$item] .= basename($item);
				}
			}
		}
		return $a;
	}

	public function toURI($path = '') {
		$path = realpath($path);
		if ($path == '' or $path < $this -> path) {
			$path = $this -> path;
		}
		$co = "/" . str_replace('/', '\/', $this->DOM) . "/A";
		if (preg_match($co, $this->PRACA)) {
			$path = str_replace($this->DOM, $this->URI, $path);
		} elseif (!preg_match($co, $path)) {
			$end=$this->URI . $this->TU;
			$path = str_replace($this->PRACA, dirname($this->TEN), $path);
		}

		return str_replace(" ", "%20", $path);
		/*
		 * pozmieniać i weryfikować wykonane;
		 */
	}

	public function splitPath($path = '') {
		if (empty($path)) {
			$path = (is_dir($this -> plik)) ? $this -> plik : dirname($this -> plik);
		}
		$a = explode('/', $path);
		$end = "<ul>";
		$e = '';
		foreach ($a as $key => $val) {
			$e .= $val . DIRECTORY_SEPARATOR;
			if ($e >= $this -> path and file_exists($e)) {
				$end .= "<li ><a href=\"" . $this->TEN . substr($e, strlen($this -> path), -1) . "\"> $val </a></li> ";
			} else {
				continue;
			}
		}
		$end .= "</ul>";
		return $end;
	}

	function queryString($params, $name = null) {
		$ret = "";
		foreach ($params as $key => $val) {
			if (is_array($val)) {
				if ($name == null)
					$ret .= queryString($val, $key);
				else
					$ret .= queryString($val, $name . "[$key]");
			} else {
				if ($name != null)
					$ret .= $name . "[$key]" . "=$val&";
				else
					$ret .= "$key=$val&";
			}
		}
		return $ret;
	}

}
?>