<?php 
/**
 * 
 */
namespace CMS;
class PokazPliki extends Pliki {

public function pokazListe($co=DIR,$path=''){
		if (empty($path)) {
			$path=(is_dir($this->plik))?$this->plik:dirname($this->plik);
		}
	$lista=$this->lista(realpath($path));
	$i=0;
	$j=0;
	$z = "<ul>";
	if(!empty($lista)){
	foreach ($this->lista(realpath($path)) as $name => $link) {
		if (file_exists($link)) {
		switch ($link) {
			case is_dir($link) and $link!='' and $co==DIR:
				++$i;	
				$z.=$this->pokaz_pomoc($name, $link);
			break;
			case is_file($link) and $link!='' and $co==PLIK:
				++$j;
				$z.=$this->pokaz_pomoc($name, $link);
			break;
		}
		} else {
			$z.=$this->pokaz_pomoc($name, $link);
		}
	} 
	} else {
		$z.="<li>{$this->lang['brak']} {$this->lang['plików']} {$this->lang['i']} {$this->lang['katalogów']}</li>";
		--$i;
		--$j;
	}
	($i==0 and $co==DIR)?$z.="<li>{$this->lang['brak']} {$this->lang['katalogów']}</li>":"<li>$i</li>";
	($j==0 and $co==PLIK)?$z.="<li>{$this->lang['brak']} {$this->lang['plików']}</li>":"<li>$j</li>";
	$z.="</ul>";
	return $z;
}
private function pokaz_pomoc($name,$link)
{
	$akcja=($this->LINK)?$this->TEN.$this->LINK:$this->TEN;
	$go=$akcja.DIRECTORY_SEPARATOR;
	$go=(is_dir($this->plik))?$go:dirname($go).DIRECTORY_SEPARATOR;	
	$operacje=array(
		"&nbsp;<a href=\"".substr($go, 0,-1)."?usun=$name\"><button>{$this->lang['usun']}</button></a>",
		"&nbsp;<a href=\"".substr($go, 0,-1)."?zaznacz=$name\"><button>{$this->lang['zaznacz']}</button></a>",
		"&nbsp;<a href=\"".substr($go, 0,-1)."?stary=$name\"><button>{$this->lang['nazwa']}</button></a>",
		"&nbsp;<a href=\"".substr($go, 0,-1)."?old=$name\"><button>{$this->lang['zmień']}</button></a>",
		"&nbsp;<a href=\"".substr($go, 0,-1)."?w=3\"><button>{$this->lang['odznacz']}</button></a>"
	);
	switch ($link) {
		case is_readable($link) and  is_writable($link) :
			$przyciski=$operacje[1].$operacje[2].$operacje[3].$operacje[0];
		break;
		case is_readable($link):
			$przyciski=$operacje[1].$operacje[3].$operacje[0];	
		break;
		case is_writable($link):
			$przyciski=$operacje[1].$operacje[2].$operacje[3].$operacje[0];	
		break;
	}
	
	if ($_GET['stary']==$name) {
		$przyciski=$this->formularz('nazwa');
	}
	if ($_SESSION['zaznacz']==$link or $_REQUEST['zaznacz']==$name) {
		$przyciski=$operacje[4];
	}
	$pokaz_link = "<li><a href=\"$go$name\">$name</a><div>$przyciski</div></li>";
	if(!is_readable($link) or !file_exists($link)) {
		$przyciski=$operacje[0].$operacje[3];
		$pokaz_link = "<li>{$this->lang['nie']} {$this->lang['można']} {$this->lang['odczytać']} <div>$przyciski</div></li>";	
	} 
return $pokaz_link;	
}
public function pokazPlik($path=''){
	
	$path=($path=='')?$this->plik:$path;
	$akcja=($this->LINK)?$this->TEN.$this->LINK:$this->TEN;
	$go=(is_dir($this->plik))?$akcja:dirname($akcja);
	$name=basename($path);
	if(!file_exists($path)) {
		return "<b>{$this->lang['nie']} {$this->lang['ma']} {$this->lang['takiego']} {$this->lang['pliku']}</b>";
	} 
	if(!is_file($path)){
		return "<b>{$this->lang['nie']} {$this->lang['wybrano']} {$this->lang['pliku']}</b>";
	}
	if (is_readable($path)) {
		$usun="&nbsp;<a href=\"".$go."?usun=$name\"><button>{$this->lang['usun']}</button></a>";
		$zaznacz="&nbsp;<a href=\"".$akcja."?zaznacz=$name\"><button>{$this->lang['zaznacz']}</button></a>";
		$zmien="&nbsp;<a href=\"".$akcja."?stary=$name\"><button>{$this->lang['nazwa']} {$this->lang['pliku']}</button></a>";
		$chmod="&nbsp;<a href=\"".substr($go, 0,-1)."?old=$name\"><button>{$this->lang['zmień']}</button></a>";
		if (!is_writable($path)) {
			$zapis= "<b>{$this->lang['nie']} {$this->lang['do']} {$this->lang['zapisu']}</b>";
			$form="<p>$zapis</p><p>$zaznacz $zmien $usun</p>";
		} else {
			$form= "<p>$zaznacz $zmien $chmod $usun </p>";
		}
		$img_t=dirname($this->TEN)."/helper/obrazek.helper.php?img=$path";
		$img="data://".mime_content_type($path).";base64,".base64_encode(file_get_contents($img_t));
		$z="<div><b>{$this->lang['nazwa:']} <a href=\"".dirname($this->TEN)."/helper/pobierz.helper.php?pobierz=".$path."\">".basename($path)."</a> {$this->lang['rozmiar:']} ". round(filesize($path) / 1024,2) ." kB, {$this->lang['typ']}:  ".mime_content_type($path). "</b></div>";
		//$usun.$zaznacz.$zmien
		if ($this->typPliku($path)=="text") {
			$z.="<div>$form<pre>".file_get_contents($path)."</pre></div>";
		} elseif ($this->typPliku($path)=="img") {
			$z.="<div>$form<img src=\"".$img."\" alt=\"".basename($path)."\"/></div>";
		} elseif ($this->typPliku($path)=="pusty") {
			$z.="<b>$form{$this->lang['plik']} {$this->lang['jest']} {$this->lang['pusty']}</b>";
		} else {
			$z.="<b>$form{$this->lang['nie']} {$this->lang['ten']} {$this->lang['typ']}</b>";
		}
	}  else {
		$z="<b>{$this->lang['nie']} {$this->lang['można']} {$this->lang['odczytać']} ".basename($path)."</b>";
	}
	return $z;
}
private function typPliku($plik='')
{
	$typ=mime_content_type($plik);
	$text="/^text/";
	$xml='/xml/';
	$empty='/x-empty/';
	$image="/^image\//";
	$b=preg_match($text, $typ);
	$c=preg_match($xml, $typ);
	$d=preg_match($image, $typ);
	$e=preg_match($empty, $typ);
	if ($b) {
		$a='text';
	} elseif ($c) {
		$a='text';
	} elseif ($d) {
		$a='img';
	} elseif ($e) {
		$a='pusty';
	} else {
		$a='nieznany';
	}
	
	return $a;
}
private function nowyplik($nazwa='',$co=KAT){

	switch ($co) {
		case KAT:
			if(file_exists($nazwa)) {
				$a=$this->lang['katalog']." ".basename($nazwa)." ".$this->lang['już']." ".$this->lang['istnieje'];
			} elseif (!is_writable(dirname($nazwa))) {
				$a=$this->lang['katalog']." ". dirname(basename($nazwa)) ." {$this->lang['nie']} {$this->lang['do']} {$this->lang['zapisu']}";
			} else {
				mkdir($nazwa,octdec($this->konfig['core']['chmod_katalogi'])); 
				$a=$this->lang['utworzono']." ". basename(basename($nazwa));
			}
			break;
		 
		case PLIK:
			if(file_exists($nazwa)) {
				$a=$this->lang['plik']." ".basename($nazwa)." $this->lang['już']} {$this->lang['istnieje']}";
			}elseif (!is_writable(dirname($nazwa))) {
				$a=$this->lang['katalog']." ". dirname(basename($nazwa)) ." {$this->lang['nie']} {$this->lang['jest']} {$this->lang['do']} {$this->lang['zapisu']}";
			} else {
				$plik=fopen($nazwa, "w"); 
				fclose($plik); 
				chmod($nazwa,octdec($this->konfig['core']['chmod_pliki']));
				$a=$this->lang['utworzono']." ". basename(basename($nazwa));
			}
			break;
	}
	return $a;
}
private function usun($co) {
	if (!file_exists($co)) {
		return "{$this->lang['plik']} {$this->lang['nie']} {$this->lang['istnieje']}";
	}
	if(is_dir($co)){
	foreach(glob($co . '/*') as $usun) {
		if(is_dir($usun))
			$this->usun($usun);
		else
			unlink($usun);
	}
	rmdir($co);
	} else {
		unlink($co);
	}
	$a=$this->lang['usunięto']." ".basename($co);
	return $a;
}
private function kopiuj($co='',$gdzie='',$com=0)
{
	$gdzies=$gdzie.DIRECTORY_SEPARATOR.basename($co);
	if (is_dir($co)) {
		mkdir($gdzie);
	$files = scandir($co);	
	foreach ($files as $file){
     if ($file != "." && $file != "..") {
       	$this->kopiuj("$co/$file", "$gdzie/$file"); 
	} else {
		//copy("$co/$file", "$gdzie/$file");
		$data = file_get_contents("$co/$file");
		file_put_contents("$gdzie/$file", $data);
	}
	}
	} elseif(is_file($co)) {
		$data = file_get_contents($co);
		file_put_contents($gdzie, $data);
	}
	if ($com==1) {
		$this->usun($co);
	}
}
private function przenies($co,$gdzie)
{
	return $this->kopiuj($co,$gdzie,1);	
}
private function przenies_sys($co='',$gdzie='',$com=0)
{
$com=($com==0)?'mv':'cp -R';
$cmd = ''.$com.' "'.$co.'" "'.$gdzie.'"'; 
exec($cmd, $output, $return_val); 

if ($return_val == 0) { 
	return "{$this->lang['udane']}"; 
} else { 
	return "{$this->lang['nie']} {$this->lang['udane']}";
}
return $co." > ".$gdzie;
}

public function formularz($jaki='nowy')
{
	$args = func_get_args(); 
	$path=(is_dir($this->plik))?$this->plik:dirname($this->plik);
	$akcja=($this->LINK)?$this->TEN.$this->LINK:$this->TEN;
	$inputs=$_REQUEST['stary'];
	$inputn=$_REQUEST['nowy'];
	$form=array(
	" <form action=\"$akcja\" method=\"POST\" enctype=\"multipart/form-data\">\n",
	"</form> \n"
	);
	$text=array(
	"<input type=\"text\" name=\"nowy\" />\n",
	"{$this->lang['stara']} {$this->lang['nazwa']}&nbsp;<b>{$_GET['stary']},</b>&nbsp;{$this->lang['nowa']} {$this->lang['nazwa']}<input type=\"text\" name=\"inny\" value=\"{$_GET['stary']}\"/>\n"
	);
	$inne=array(
	"{$this->lang['nowy']}&nbsp;{$this->lang['katalog']}&nbsp;<input type=\"radio\" name=\"co\" value=\"KAT\" checked=\"checked\" />\n",
	"{$this->lang['plik']}&nbsp;<input type=\"radio\" name=\"co\" value=\"PLIK\"  />\n",
	"{$this->lang['wybierz']} {$this->lang['Plik']}<input type=\"file\" name=\"plik\" />\n"
	);
	$submit=array(
	"<input type=\"submit\" value=\"{$this->lang['wgraj']}\" />\n",
	"<input type=\"submit\" value=\"{$this->lang['zmień']}\" />\n",
	"<input type=\"submit\" value=\"{$this->lang['utwórz']}\" />\n"
	);
	
	switch ($jaki) {
		case 'nowy':
			if(!isset($_REQUEST['co'])){
				$return=$form[0].$inne[0].$inne[1].$text[0].$submit[2].$form[1];
			} else {
				$co=$_REQUEST['co'];
				$return=$form[0].$inne[0].$inne[1].$text[0].$submit[2].$form[1];
				$return.=$this->nowyplik($path.DIRECTORY_SEPARATOR.$inputn,$co);
			}
		break;
		case 'usun':
			if(isset($_REQUEST['usun'])){
				$co =$_REQUEST['usun'];
				$return=$this->usun($this->plik.DIRECTORY_SEPARATOR.$co);
			} 
		break;
		case 'przenies':
			if(isset($_REQUEST['zaznacz'])){
				$co =$_REQUEST['zaznacz'];
				$link=$this->plik.DIRECTORY_SEPARATOR.$co;
				$link=(is_dir($this->plik))?$link:dirname($link);
				$_SESSION['zaznacz']=$link;
				$return= "<span>{$this->lang['zaznaczono']} <b>\"".basename($_SESSION['zaznacz'])."\"</b><span>";
				$return.="&nbsp;&nbsp;<a href=\"".$this->TEN.substr($path, strlen($this->path))."?w=3\"><button>{$this->lang['odznacz']}</button></a>";
			} elseif ($_SESSION['zaznacz']!='') {
				$ses=$_SESSION['zaznacz'];
				$return="<span>{$this->lang['zaznaczono']} <b>\"".basename($ses)."\"</b>&nbsp;</span>";
				$return.="&nbsp;&nbsp;<a href=\"".$this->TEN.substr($path, strlen($this->path))."?w=1\"><button>{$this->lang['wklej']}</button></a>";
				$return.="&nbsp;&nbsp;<a href=\"".$this->TEN.substr($path, strlen($this->path))."?w=2\"><button>{$this->lang['kopiuj']}</button></a>";
				$return.="&nbsp;&nbsp;<a href=\"".$this->TEN.substr($path, strlen($this->path))."?w=3\"><button>{$this->lang['odznacz']}</button></a>";
			if ($_REQUEST['w']==1) {
				if(!file_exists($path.DIRECTORY_SEPARATOR.basename($ses))){
				$return=$this->przenies($ses,$path.DIRECTORY_SEPARATOR.basename($ses),1);
				$_SESSION['zaznacz']='';
				} else return "{$this->lang['nie']} {$this->lang['udane']}";
			} elseif ($_REQUEST['w']==2) {
				if(!file_exists($path.DIRECTORY_SEPARATOR.basename($ses))){
				$return=$this->kopiuj($ses,$path.DIRECTORY_SEPARATOR.basename($ses));
				$_SESSION['zaznacz']='';
				} else return "{$this->lang['nie']} {$this->lang['udane']}";
			} elseif ($_REQUEST['w']==3) {
				$_SESSION['zaznacz']='';
				return $this->lang['odznaczono'];
			} 
		}		
		break;
		case 'nazwa':
			if(!isset($_REQUEST['inny']) and isset($_GET['stary'])){
			$_SESSION['stary']=$_GET['stary'];
			$return=$form[0].$text[1].$submit[1].$form[1];
			
			} elseif(isset($_SESSION['stary']) and isset($_REQUEST['inny']) and $_SESSION['stary']!=''){
				$stara=$_SESSION['stary'];
				$nowa=$_REQUEST['inny'];
					if(!file_exists($path.DIRECTORY_SEPARATOR.$nowa) and is_writable($path)){
					if(is_writable($path.DIRECTORY_SEPARATOR.$stara)){
						rename($path.DIRECTORY_SEPARATOR.$stara ,$path.DIRECTORY_SEPARATOR.$nowa);
						$_SESSION['stary']='';
						$return="{$this->lang['udane']} \"$stara\" {$this->lang['zmieniono']} {$this->lang['na']} \"$nowa\"";
						$return.="&nbsp;<a href=\"$akcja\"><button>{$this->lang['odśwież']}</button></a>";
					} else {
						$_SESSION['stary']='';
						return "<b>{$this->lang['nie']} {$this->lang['udane']}</b>";
					}
					} else {
						$_SESSION['stary']='';
						return "<b>{$this->lang['nie']} {$this->lang['udane']}, {$this->lang['nazwa']} {$this->lang['już']} {$this->lang['istnieje']}</b>";
					}
			}
		break;
		case 'wgraj':
			if(!isset($_FILES['plik'])){
			$wgraj=$form[0].$inne[2].$submit[0].$form[1];
			$return=$wgraj;
			} elseif(isset($_FILES['plik'])) {
				if (!file_exists($path.DIRECTORY_SEPARATOR . $_FILES['plik']["name"]) and ($_FILES['plik']["size"] <= $this->konfig['core']['upsize'])) {
					move_uploaded_file($_FILES['plik']["tmp_name"],$path.DIRECTORY_SEPARATOR . $_FILES['plik']["name"]);
					chmod($path.DIRECTORY_SEPARATOR . $_FILES['plik']["name"], octdec($this->konfig['core']['chmod_pliki']));
					$return="{$this->lang['udane']} {$_FILES['plik']['name']} {$this->lang['rozmiar:']} ".round($_FILES['plik']['size'] / 1024,4) ."kB ";
					$return.=$wgraj;
				} elseif ($_FILES['plik']["size"] > $this->konfig['core']['upsize']) {
					$return= "{$this->lang['nie']} {$this->lang['udane']} {$_FILES['plik']['name']} ".round($_FILES['plik']['size'] / 1024,4) ."kB < ".round($this->konfig['core']['upsize'] / 1024,4) ."kB ";
					$return.=$wgraj;
				}elseif ($_FILES['plik']["error"] > 0) {
					$return= "{$this->lang['nie']} {$this->lang['udane']} : {$_FILES['plik']["error"]}";
					$return.=$wgraj;
				} 
			}
		break;
		default:
				$return="<b>{$this->lang['nie']} {$this->lang['udane']}</b>";
			break;
	}
	return $return;
}
public function szukaj($czego='',array $gdzie)
{
	$czego = preg_replace("/[*]/","[a-zA-Z0-9]",urldecode($czego));
	$i=0;
	$a="<ul>";
	foreach ($gdzie as $key=>$dane) {
		if (preg_match("/$czego/s",strtolower($dane))) {
			++$i;
			$a.="<li><a href=\"".$this->TEN.substr($key, strlen($this->path))."\">".str_replace($this->path, '', $key)."</a>&nbsp;";
			$a.=(is_dir($key))?$this->lang['katalog'] : $this->lang['plik']."</li>\n";
		} 
	}
	$a.="</ul>";
	if($i==0) {
			return "{$this->lang['plik']} {$this->lang['nie']} {$this->lang['istnieje']}";
	}
return  (string) "$a {$this->lang['znaleziono']} $i  {$this->lang['plików']}";

}
public function szukajForm($co=null)
{
	$path = ($this->LINK) ? $this->TEN . $this->LINK : $this->TEN;
	$link= (is_dir($this->plik))?$this->plik:dirname($this->plik);
	$gdzie=$this->rekursywnie($link);
	$czego=$_REQUEST['szukaj'];
	
	$form = array(
	"<form action=\"$path\" method=\"GET\">",
	"{$this->lang['szukaj']}  {$this->lang['plików']} <input type=\"text\" name=\"szukaj\" />",
	"<input type=\"submit\" value=\"{$this->lang['szukaj']}\" />\n",
	"</form>"
	);
	$szukane=$form[0].$form[1].$form[2].$form[3];
	if (isset($czego) and strlen($czego)>=$this->konfig['core']['szukane_minimum']) {
		$znalezione=$this->szukaj($czego,$gdzie);
	} elseif(isset($czego) and strlen($czego)<$this->konfig['core']['szukane_minimum']) {
		$znalezione="{$this->lang['nie']} {$this->lang['udane']}";
	}
	switch ($co) {
		case 1:
			return "<hr /> $szukane <hr />";
			break;
		case 2:
			return "<hr /> $znalezione <hr />";
			break;
		default:
			return "<hr /> $szukane <hr /> $znalezione <hr />";
			break;
	}
}
public function chmod_r($co ,$mode,$w='nie'){
	if (!file_exists($co)) {
		return "{$this->lang['plik']} {$this->lang['nie']} {$this->lang['istnieje']}";
	}
	if ($w=='tak') {
	if (is_dir($co)) {
		(@chmod($co, octdec($mode)))?$out=TRUE:$out=FALSE;
		foreach (glob($co."/*") as $value) {
			$this->chmod_r($value, $mode);
		}
	} else {
		(@chmod($co, octdec($mode)))?$out=TRUE:$out=FALSE;
	}
	} else {
		(@chmod($co, octdec($mode)))?$out=TRUE:$out=FALSE;
	}
	
	
	return $out;
}
public function chmodForm()
{
	$arr_mode=$_REQUEST['arr_mode'];
	$old=$_REQUEST['old'];
	$mode=$_REQUEST['mode'];
	$path = ($this->LINK) ? $this->TEN . $this->LINK : $this->TEN;
	$link= (is_dir($this->plik))?$this->plik:dirname($this->plik);
	$link.=DIRECTORY_SEPARATOR.$old;
	$odczyt="{$this->lang['odczyt']}";
	$zapis="{$this->lang['zapis']}";
	$exec="{$this->lang['wykonywanie']}";
	$form=array(
	"<form action=\"$path\" method=\"POST\">",
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"400\" checked=\"checked\" />",
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"200\" checked=\"checked\" />",
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"100\" />",
	
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"40\" checked=\"checked\" />",
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"20\" checked=\"checked\" />",
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"10\" />",
	
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"4\" checked=\"checked\" />",
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"2\" checked=\"checked\" />",
	"<input type=\"checkbox\" name=\"arr_mode[]\" value=\"1\" />",
	
	"{$this->lang['w']} {$this->lang['katalogu']}<input type=\"checkbox\" value=\"tak\" name=\"mode\" checked=\"checked\"/>",
	"<input type=\"hidden\"  name=\"old\" value=\"$old\" />",
	"<input type=\"submit\" value=\"{$this->lang['zmień']}\" />\n",
	"</form>"
	);
	if (isset($old)) {
	if (!isset($arr_mode)) {
		$out="<div style=\"width:160px; border: 1px solid black; padding:3px;\">";
		$out.=$form[0];
		$out.="<b>{$this->lang['dla']} : \"$old\"</b>";
		$out.="<div>$form[1]$form[4]$form[7] $odczyt</div>";
		$out.="<div>$form[2]$form[5]$form[8] $zapis</div>";
		$out.="<div>$form[3]$form[6]$form[9] $exec</div>";
	 	$out.=$form[10].$form[11];
		$out.=$form[12].$form[13];
		$out.="</div>";
	} elseif (isset($arr_mode)) {
		$i=0;
		foreach ($arr_mode as $key => $value) {
			$i += $value;
		}
		$val=$i;
	} 
	if ($mode=='tak' and isset($arr_mode)) {
		$zmien=$this->chmod_r($link,$val, 'tak');
	} elseif ($mode!='tak' and isset($arr_mode)) {
		$zmien= $this->chmod_r($link, $val);
	}
	if(isset($arr_mode)){
	if ($zmien) {
		$out="{$this->lang['zmieniono']} \"$link\" {$this->lang['na']} \"$val\"";
		$out.="&nbsp;<a href=\"$akcja\"><button>{$this->lang['odśwież']}</button></a>";
	} else {
		$out="{$this->lang['nie']} {$this->lang['zmieniono']} \"$link\"";
	}
	}
	}
	return $out;
}
}
?>